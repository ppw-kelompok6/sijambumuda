### Kelompok 6 PPW 2019

* Mauludi Afina Mirza - 1606884836

* Tri Rahayu - 1706984770

* Palito - 1706039793

* Khameela Rahmah - 1706023422

## Link Heroku

https://ppw-kelompok6.herokuapp.com/

## Status

[![pipeline status](https://gitlab.com/ppw-kelompok6/sijambumuda/badges/master/pipeline.svg)](https://gitlab.com/ppw-kelompok6/sijambumuda/commits/master)
[![coverage report](https://gitlab.com/ppw-kelompok6/sijambumuda/badges/master/coverage.svg)](https://gitlab.com/ppw-kelompok6/sijambumuda/commits/master)