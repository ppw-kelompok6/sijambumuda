from django.test import TestCase
from django.contrib.auth.models import User
from django.http import HttpRequest
from about.models import Testimony
import about.views as target
import json

# Create your tests here.

class TestUrlAvaibily(TestCase):

    def test_endpoint(self):
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)


class TestApiTestimony(TestCase):
    def setUp(self):
        self.user1 = User(
            username='tony',
            first_name='Tony',
            last_name='Stark',
            email='tony@stark.com',
        )
        self.user1.set_password('testtesttesttest')
        self.user1.save()

        self.user2 = User(
            username='rogers',
            first_name='Steve',
            last_name='Rogers',
            email='steve@shield.com',
        )
        self.user2.set_password('testtesttesttest')
        self.user2.save()

        self.testimony1 = Testimony(user=self.user2, testimony='Great! I love this library so much')
        self.testimony1.save()

    def test_load_api(self):
        request = HttpRequest()
        request.method = 'GET'
        request.user = self.user1
        result = json.loads(target.load_testimony(request).content)
        self.assertEqual(len(result), 1)

    def test_add_api_success(self):
        request = HttpRequest()
        request.POST = {'testimonyText': 'Aku sering dateng kesini'}
        request.method = 'POST'
        request.user = self.user1
        result = json.loads(target.add_testimony(request).content)
        self.assertEqual(result['message'], 'SUCCESS')

    def test_add_api_fail(self):
        request = HttpRequest()
        request.GET = {'testimonyText': 'Aku sering dateng kesini'}
        request.method = 'GET'
        request.user = self.user1
        result = json.loads(target.add_testimony(request).content)
        self.assertEqual(result['message'], 'FAIL')

