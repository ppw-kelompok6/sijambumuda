from django.urls import path
from .views import *

app_name ="about"

urlpatterns = [
    path('about/', about, name="about"),
    path('about/api/v2/load_testimony/', load_testimony, name="load_testimony"),
    path('api/v2/add_testimony/', add_testimony, name="add_testimony"),
]