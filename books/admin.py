from django.contrib import admin
from .models import BooksList

# Register your models here.
admin.site.register(BooksList)
