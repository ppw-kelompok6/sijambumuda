from django.db import models

# Create your models here.
class BooksList(models.Model):
    nomor = models.CharField(max_length=200)
    genre = models.CharField(max_length=200)
    judul = models.CharField(max_length=200)
    pengarang = models.CharField(max_length=200)
    penerbit = models.CharField(max_length=200)
    gambar = models.CharField(max_length=200)
    sinopsis = models.TextField(max_length=800)