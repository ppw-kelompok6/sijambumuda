from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import books_page
from .models import BooksList
from register.models import Member

# Create your tests here.

class BooksTest(TestCase):
    def test_books_url_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_books(self):
        new_member = Member.objects.create(name="Robert Downey", noID="10000000000", uname="ironman",
                                            email="robertdowney@aowkwkk.com", password="iamironman", alamat="marvel cinematic universe")
        new_member.save()

        # Creating books
        BooksList.objects.create(
            nomor="01",
            genre="Fiksi",
            judul="Dilan: Dia adalah Dilanku Tahun 1990",
            pengarang="Pidi Baiq",
            penerbit="PT Mizan Pustaka",
            gambar= "https://saltncookies.files.wordpress.com/2019/02/1_z7_d-zrx3-owismqrd-mta.jpeg",
            sinopsis= "Milea ketemu Dilan")
        # Retrieving all available activity
        count_status = BooksList.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_books_list_is_exist(self):
        new_member = Member.objects.create(name="Robert Downey", noID="10000000000", uname="ironman",
                                            email="robertdowney@aowkwkk.com", password="iamironman", alamat="marvel cinematic universe")
        new_member.save()

        # Creating a new news
        new_books = BooksList(
            nomor="01",
            genre="Fiksi",
            judul="Dilan: Dia adalah Dilanku Tahun 1990",
            pengarang="Pidi Baiq",
            penerbit="PT Mizan Pustaka",
            gambar= "https://saltncookies.files.wordpress.com/2019/02/1_z7_d-zrx3-owismqrd-mta.jpeg",
            sinopsis= "Milea ketemu Dilan")
        new_books.save()

        response = Client().get('/books/')
        html_response = response.content.decode('utf8')
        self.assertIn("Fiksi", html_response)