from django.urls import path
from . import views

app_name ="books"

urlpatterns = [
    path('books/', views.books_page, name="books"),
]