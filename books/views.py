from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import BooksList

# Create your views here.

def get_context_books(context, cnt):
    context['books_datas'] = []
    the_books = BooksList.objects.all()[:cnt]
    for a_books in the_books:
        context['books_datas'].append({
        'nomor': a_books.nomor,
        'genre': a_books.genre,
        'judul': a_books.judul,
        'pengarang': a_books.pengarang,
        'penerbit': a_books.penerbit,
        'gambar': a_books.gambar,
        'sinopsis': a_books.sinopsis})
    return context

def books_page(request):
    context = {}
    get_context_books(context, BooksList.objects.all().count())
    return render(request, 'books.html', context)