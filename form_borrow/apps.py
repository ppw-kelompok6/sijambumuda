from django.apps import AppConfig


class FormBorrowConfig(AppConfig):
    name = 'form_borrow'
