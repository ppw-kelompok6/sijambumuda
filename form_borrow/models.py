from django.db import models
from datetime import date
from register.models import Member
from books.models import BooksList

# Create your models here.
class BorrowBooks(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    no_buku = models.ForeignKey(BooksList, on_delete=models.CASCADE)
    tanggal = models.DateField()