from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import formborrow
from .models import BorrowBooks
from register.models import Member
from books.models import BooksList
import datetime
# Create your tests here.

class FormBorrowTest(TestCase):
    def test_formborrow_url_is_exist(self):
        response = Client().get('/form_borrow/')
        self.assertEqual(response.status_code, 200)

    def setUp(self):
        self.member1 = Member(name="Steve Rogers", noID="101010101010", uname="captainamerica",
                              email="steverogers@aowkwkk.com", password="icandothisallday", alamat="on earth")
        self.member1.save()

        self.member2 = Member(name="Thor", noID="11010101000", uname="thorsonofodin",
                              email="thor@aowkwkk.com", password="godofthunder", alamat="asgard")
        self.member2.save()

        self.books = BooksList(
            nomor="01",
            genre="Fiksi",
            judul="Dilan: Dia adalah Dilanku Tahun 1990",
            pengarang="Pidi Baiq",
            penerbit="PT Mizan Pustaka",
            gambar= "https://saltncookies.files.wordpress.com/2019/02/1_z7_d-zrx3-owismqrd-mta.jpeg",
            sinopsis= "Milea ketemu Dilan")
        self.books.save()

    def test_success_borrow(self):
        initialBorrowBooksCount = BorrowBooks.objects.all().count()
        self.client.post('/form_borrow/', {'uname': 'captainamerica',
                                      'email': 'steverogers@aowkwkk.com',
                                      'nomor': '01',
                                      'tanggal': '2019-03-24'})
        count_status = BorrowBooks.objects.all().count()
        self.assertEqual(count_status - initialBorrowBooksCount, 1)

    def test_books_number_not_avaiable(self):
        response = self.client.post('/form_borrow/', {'uname': 'captainamerica',
                                      'email': 'steverogers@aowkwkk.com',
                                      'nomor': '123',
                                      'tanggal': '2018-03-24'})
        self.assertEqual(response.status_code, 500)

    def test_username_email_doesnt_match(self):
        response = self.client.post('/form_borrow/', {'uname': 'captainamerica',
                                      'email': 'thor@aowkwkk.com',
                                      'nomor': '01',
                                      'tanggal': '2018-03-24'})
        self.assertEqual(response.status_code, 500)