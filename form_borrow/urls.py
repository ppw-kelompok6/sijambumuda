from django.urls import path
from . import views

app_name ="form_borrow"

urlpatterns = [
    path('form_borrow/', views.formborrow, name="formborrow"),
]