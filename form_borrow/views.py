from django.shortcuts import render
from django.http import HttpResponseServerError
from .models import BorrowBooks
from register.models import Member
from books.models import BooksList

# Create your views here.

R_500_SERVER_ERROR = 'RESPONSE CODE 500 SERVER ERROR: '
USERNAME_EMAIL_DOESNT_MATCH = 'USERNAME_EMAIL_DOESNT_MATCH'
BOOKS_NUMBER_NOT_AVAIABLE = 'BOOKS_NUMBER_NOT_AVAIABLE'

def get_member(uname, email):
    return Member.objects.get(uname=uname, email=email)

def get_books_number(nomor):
    return BooksList.objects.get(nomor=nomor)

def formborrow(request):
    context = {'message': ''}
    if (request.method == 'POST'):
        try:
            member = get_member(request.POST['uname'], request.POST['email'])
        except Member.DoesNotExist:
            return HttpResponseServerError(R_500_SERVER_ERROR + USERNAME_EMAIL_DOESNT_MATCH)
        try:
            no_buku = get_books_number(request.POST['nomor'])
        except BooksList.DoesNotExist:
            return HttpResponseServerError(R_500_SERVER_ERROR + BOOKS_NUMBER_NOT_AVAIABLE)
        newBorrowBooks = BorrowBooks(
            member=member,
            no_buku=no_buku,
            tanggal=request.POST['tanggal']
        )
        newBorrowBooks.save()
        context['message'] = 'Berhasil Merekam Peminjaman!'

    return render(request, 'form.html', context)
