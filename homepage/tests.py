from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home_page
# Create your tests here.

class Kelompok6(TestCase):
    def test_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_homepage_has_comingsoon(self):
        response = Client().get('/')
        home_page_content = 'sijambumuda.'
        html_response = response.content.decode('utf8')
        self.assertIn(home_page_content, html_response)