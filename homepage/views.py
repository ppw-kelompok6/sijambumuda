from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.

def home_page(request):
    response = {}
    return render(request, 'homepage.html', response)