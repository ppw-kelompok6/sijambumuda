from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.apps import apps
from .views import records_page
from .apps import RecordsConfig

# Create your tests here.

class RecordsUnitTest(TestCase):
	def test_records_url_is_exist(self):
		response = Client().get('/records/')
		self.assertEqual(response.status_code, 200)