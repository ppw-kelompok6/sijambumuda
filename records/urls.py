from django.urls import path
from . import views

app_name ="records"

urlpatterns = [
    path('records/', views.records_page, name="records_page"),
]