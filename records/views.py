from django.shortcuts import render
from form_borrow.models import BorrowBooks
from datetime import timedelta
from datetime import date
# Create your views here.

response = {}

def records_page(request):
	BookList = list(BorrowBooks.objects.all())
	notReturned = list()
	for x in BookList:
		temptime = x.tanggal + timedelta(days=14)
		if(date.today() < temptime):
			notReturned.append(x)
	notReturned.reverse()
	response['peminjaman'] = notReturned
	return render(request, 'records.html', response)
