from django.db import models
from datetime import date

# Create your models here.

class Member(models.Model):
    name = models.CharField(max_length=200)
    noID = models.CharField(max_length=200)
    uname = models.CharField(max_length=200)
    email = models.EmailField(max_length = 200, unique = True)
    password = models.CharField(max_length = 200)
    alamat = models.CharField(max_length=200)


