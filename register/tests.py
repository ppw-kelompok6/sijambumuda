from django.test import TestCase
from django.test import Client
from .models import Member


# Create your tests here.
class RegisterTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_can_create_model(self):
        new_register = Member.objects.create(name="Robert Downey", noID="10000000000", uname="ironman",
                                            email="robertdowney@aowkwkk.com", password="iamironman", alamat="marvel cinematic universe")
        new_register.save()
        count_register = Member.objects.all().count()
        self.assertEqual(count_register, 1)

    def test_can_register_member_from_input_form(self):
        self.client.post("/register/", {"name": "Robert Downey", "noID": "10000000000", "uname" : "ironman",
                                        "email": "robertdowney@aowkwkk.com", "password": "iamironman", "alamat":"marvel cinematic universe"})
        count_register = Member.objects.all().count()
        self.assertEqual(count_register, 1)

    def test_email_already_taken(self):
        response = self.client.post(
            "/register/", {"name": "Robert Downey",
                           "noID": "10000000000",
                           "uname" : "ironman",
                           "email": "robertdowney@aowkwkk.com",
                           "password": "iamironman",
                           "alamat":"marvel cinematic universe"})
        response = self.client.post(
            "/register/", {"name": "Robert Downey",
                           "noID": "10000000000",
                           "uname" : "ironman",
                           "email": "robertdowney@aowkwkk.com",
                           "password": "iamironman",
                           "alamat":"marvel cinematic universe"})
        html_response = response.content.decode('utf8')
        self.assertIn('Pendaftaran Gagal, E-mail Sudah Digunakan!', html_response)