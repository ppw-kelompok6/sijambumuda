from django.urls import path
from .views import index

app_name ="register"

urlpatterns = [
    path('register/', index, name="register"),
]