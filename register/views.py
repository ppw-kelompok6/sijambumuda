from django.shortcuts import render
from .models import Member

# Create your views here.

EMAIL_ALREADY_TAKEN = 'Pendaftaran Gagal, E-mail Sudah Digunakan!'
SUCCESS_REGISTER = 'Pendaftaran Berhasil!'

def index(request):
    context = {'message': ''}
    if request.method == 'POST':
        if Member.objects.filter(email=request.POST['email']).count() > 0:
            context['message'] = EMAIL_ALREADY_TAKEN
        else:
            context['message'] = SUCCESS_REGISTER
            newMember = Member(
                name=request.POST['name'],
                noID=request.POST['noID'],
                uname=request.POST['uname'],
                email=request.POST['email'],
                password=request.POST['password'],
                alamat=request.POST['alamat']
            )
            newMember.save()

    return render(request, 'register.html', context)

